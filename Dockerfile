FROM amd64/python:3.7.6-alpine
RUN mkdir app
RUN chmod -R 755 /app
WORKDIR /app
COPY . /app
EXPOSE 8000
EXPOSE 5000
ENV FLASK_APP=cheap_flights_api
RUN pip install -r requirements.txt
