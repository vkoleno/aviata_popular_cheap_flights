from collections import defaultdict

from cache_tools.cache_refresher import FlightDirection


TEST_DATA = {
    'test_get_dates_for_request': {
        'expected': ('23/03/2020', '22/04/2020'),
    },
    'test_get_direction_dict_key': {
        'input': FlightDirection('Privet', 'Poka'),
        'expected': 'Privet-Poka',
    },
    'test_update_cache_with_directions': {
        'input': [FlightDirection('Privet', 'Poka'), FlightDirection('Hello', 'World')],
        'expected': ['Privet-Poka', 'Hello-World'],
    },
    'test_add_direction_cheapest_price_to_empty_cache': {
        'input': {
            'flight_direction': FlightDirection('Privet', 'Poka'),
            'available_flights': [{
                'dTimeUTC': 1584975600,
                'price': 20,
                'booking_token': 'some_token',
            }]
        },
        'expected': {
            'Privet-Poka': {
                '23/03/2020': {
                    'price': 20,
                    'booking_token': 'some_token',
                }
            }
        }
    },
    'test_add_direction_cheapest_price_new_price_lower': {
        'input': {
            'cache': {
                'Privet-Poka': {
                    '23/03/2020': {
                        'price': 20,
                        'booking_token': 'some_token',
                    }
                }
            },
            'flight_direction': FlightDirection('Privet', 'Poka'),
            'available_flights': [{
                'dTimeUTC': 1584975600,
                'price': 19,
                'booking_token': 'some_token',
            }]
        },
        'expected': {
            'Privet-Poka': {
                '23/03/2020': {
                    'price': 19,
                    'booking_token': 'some_token',
                }
            }
        }
    },
    'test_add_direction_cheapest_price_to_cache_flight_invalid': {
        'input': {
            'flight_direction': FlightDirection('Privet', 'Poka'),
            'available_flights': [{
                'dTimeUTC': 1584975600,
                'price': 20,
                'booking_token': 'some_token',
            }]
        },
        'expected': {
            'Privet-Poka': defaultdict(),
        }
    },
    'test_add_direction_cheapest_price_new_price_higher': {
        'input': {
            'cache': {
                'Privet-Poka': {
                    '23/03/2020': {
                        'price': 20,
                        'booking_token': 'some_token',
                    }
                }
            },
            'flight_direction': FlightDirection('Privet', 'Poka'),
            'available_flights': [{
                'dTimeUTC': 1584975600,
                'price': 21,
                'booking_token': 'some_token',
            }]
        },
        'expected': {
            'Privet-Poka': {
                '23/03/2020': {
                    'price': 20,
                    'booking_token': 'some_token',
                }
            }
        }
    }
}
