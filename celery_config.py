from celery import Celery
from celery.schedules import crontab
from constants import REDIS_HOST, REDIS_PORT


app = Celery(
    'celery_config',
    broker=f'redis://{REDIS_HOST}:{REDIS_PORT}',
    include=['cache_refresher', 'cache_validator'],
)

app.conf.beat_schedule = {
    'cache_refresh': {
        'task': 'cache_refresher.run',
        'schedule': crontab(minute=0, hour=0),
    },
    'cache_validate': {
        'task': 'cache_validator.run',
        'schedule': crontab(minute='*/30'),
    }
}
