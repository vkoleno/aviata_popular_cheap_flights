from freezegun import freeze_time

from cache_tools.cache_refresher import get_dates_for_request, get_direction_dict_key, update_cache_with_directions, add_direction_cheapest_price_to_cache
from test_data import TEST_DATA


class TestCheapPopularDirections:
    """I don't consider given test as full or enough coverage. Suggested set is demo in context of test task"""

    @freeze_time('23/03/2020')
    def test_get_dates_for_request(self):
        date_start, date_end = get_dates_for_request()

        expected = TEST_DATA['test_get_dates_for_request']['expected']
        assert date_start == expected[0]
        assert date_end == expected[1]

    def test_get_direction_dict_key(self):
        flight_direction_dict_key = get_direction_dict_key(TEST_DATA['test_get_direction_dict_key']['input'])
        assert flight_direction_dict_key == TEST_DATA['test_get_direction_dict_key']['expected']

    def test_update_cache_with_directions(self):
        cache = {}
        directions = TEST_DATA['test_update_cache_with_directions']['input']
        update_cache_with_directions(directions, cache)
        assert list(cache.keys()) == TEST_DATA['test_update_cache_with_directions']['expected']

    @freeze_time('23/03/2020')
    def test_add_direction_cheapest_price_to_empty_cache(self, mocker):
        mocker.patch('cache_tools.cache_refresher.check_flight_is_valid', return_value=True)

        cache = {}
        input_data = TEST_DATA['test_add_direction_cheapest_price_to_empty_cache']['input']
        flight_direction = input_data['flight_direction']
        available_flights = input_data['available_flights']

        update_cache_with_directions([flight_direction], cache)

        add_direction_cheapest_price_to_cache(flight_direction, available_flights, cache)
        assert cache == TEST_DATA['test_add_direction_cheapest_price_to_empty_cache']['expected']

    @freeze_time('23/03/2020')
    def test_add_direction_cheapest_price_new_price_lower(self, mocker):
        mocker.patch('cache_tools.cache_refresher.check_flight_is_valid', return_value=True)

        input_data = TEST_DATA['test_add_direction_cheapest_price_new_price_lower']['input']
        cache = input_data['cache']
        flight_direction = input_data['flight_direction']
        available_flights = input_data['available_flights']

        add_direction_cheapest_price_to_cache(flight_direction, available_flights, cache)
        assert cache == TEST_DATA['test_add_direction_cheapest_price_new_price_lower']['expected']

    @freeze_time('23/03/2020')
    def test_add_direction_cheapest_price_new_price_higher(self, mocker):
        mocker.patch('cache_tools.cache_refresher.check_flight_is_valid', return_value=True)

        input_data = TEST_DATA['test_add_direction_cheapest_price_new_price_higher']['input']
        cache = input_data['cache']
        flight_direction = input_data['flight_direction']
        available_flights = input_data['available_flights']

        add_direction_cheapest_price_to_cache(flight_direction, available_flights, cache)
        assert cache == TEST_DATA['test_add_direction_cheapest_price_new_price_higher']['expected']

    @freeze_time('23/03/2020')
    def test_add_direction_cheapest_price_to_cache_flight_invalid(self, mocker):
        mocker.patch('cache_tools.cache_refresher.check_flight_is_valid', return_value=False)

        cache = {}
        input_data = TEST_DATA['test_add_direction_cheapest_price_to_cache_flight_invalid']['input']
        flight_direction = input_data['flight_direction']
        available_flights = input_data['available_flights']

        update_cache_with_directions([flight_direction], cache)

        add_direction_cheapest_price_to_cache(flight_direction, available_flights, cache)
        assert cache == TEST_DATA['test_add_direction_cheapest_price_to_cache_flight_invalid']['expected']
