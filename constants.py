from datetime import datetime

from decouple import config


# item[2] as flag that backwards direction is also needed
FLIGHT_DIRECTIONS = (
    ('ALA', 'TSE', True),
    ('ALA', 'MOW', True),
    ('ALA', 'CIT', True),
    ('TSE', 'MOW', True),
    ('TSE', 'LED', True),
)

SKYPICKER_API_HOST = 'https://api.skypicker.com'
SKYPICKER_FLIGHTS_ENDPOINT = f'{SKYPICKER_API_HOST}/flights'

BOOKING_API_HOST = 'https://booking-api.skypicker.com'
BOOKING_CHECK_FLIGHTS_ENDPOINT = f'{BOOKING_API_HOST}/api/v0.1/check_flights'
BOOKING_API_TEMPLATE_PARAMS = {
    'v': 2,
    'bnum': 0,
    'pnum': 1,
}

PARTNER_ID = config('PARTNER_ID', default='zbozletestwork')

DATE_FORMAT = '%d/%m/%Y'

FLIGHTS_PRICE_CACHE_NAME = 'cheapest_flights_state'

DAYS_CREATE_CACHE_FOR = 30

LOG_FORMAT = '%(asctime)-15s %(levelname)s %(message)s'

LAST_RUN_STATE_KEY = 'last_run_date'
CACHE_KEY = 'cheapest_flights_cache'

REDIS_HOST = config('REDIS_HOST', default='localhost')
REDIS_PORT = config('REDIS_PORT', default=6379)

LOGS_DIR = 'logs'
LOG_FILE_NAME = f'{datetime.utcnow().strftime(DATE_FORMAT).replace("/", "")}'
