import json

from constants import REDIS_HOST, REDIS_PORT, CACHE_KEY
from flask import Flask

from utils.state import State
from utils.storage import RedisStorage, BaseStorage


app = Flask(__name__)


@app.route('/get', methods=['GET'])
def get_cheapest_prices():
    state_storage: BaseStorage = RedisStorage(REDIS_HOST, REDIS_PORT)
    state: State = State(state_storage)
    cache_json: str = state.get_state(CACHE_KEY)
    cache: dict = {}
    if cache_json:
        cache: dict = json.loads(cache_json)
    return cache


if __name__ == '__main__':
    app.run(host='0.0.0.0')
