"""Module sets up logger for writing to stdout and file"""


import logging
import os

from constants import *


log_file_path: str = os.path.join(LOGS_DIR, LOG_FILE_NAME)

log_file_handler: logging.FileHandler = logging.FileHandler(log_file_path, 'a', 'utf-8')
formatter = logging.Formatter(LOG_FORMAT)

# Otherwise log file in container created by logging non-writable
os.chmod(log_file_path, 0o667)

log_file_handler.setFormatter(formatter)
log_file_handler.setLevel(logging.INFO)

logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(log_file_handler)
