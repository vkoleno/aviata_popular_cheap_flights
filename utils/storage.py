import abc
import json

import redis

from utils.logger import logger
from constants import FLIGHTS_PRICE_CACHE_NAME


class BaseStorage:
    @abc.abstractmethod
    def save_state(self, state: dict) -> None:
        """Save state into persistent storage"""
        pass

    @abc.abstractmethod
    def retrieve_state(self) -> dict:
        """Load state from persistent storage"""
        pass


class RedisStorage(BaseStorage):
    def __init__(self, redis_host, redis_port):
        self.redis = redis.Redis(host=redis_host, port=redis_port)

    def save_state(self, state: dict) -> None:
        self.redis.set(FLIGHTS_PRICE_CACHE_NAME, json.dumps(state))

    def retrieve_state(self) -> dict:
        try:
            return json.loads(self.redis.get(FLIGHTS_PRICE_CACHE_NAME).decode('utf-8'))
        except AttributeError:
            logger.warning('Storage is empty. Should be available after next refresh at 00:00 UTC')
            return {}
