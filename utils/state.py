from utils.storage import BaseStorage
from typing import Any


class State:

    def __init__(self, storage: BaseStorage):
        self.storage = storage
        self.state = self.retrieve_state()

    def retrieve_state(self) -> dict:
        return self.storage.retrieve_state() or {}

    def set_state(self, key: str, value: Any) -> None:
        """Set state for given key"""
        self.state[key] = value

        self.storage.save_state(self.state)

    def get_state(self, key: str) -> Any:
        """Get state for given key"""
        return self.state.get(key)
