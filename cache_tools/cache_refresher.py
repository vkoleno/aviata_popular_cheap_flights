import json
from collections import namedtuple, defaultdict
from datetime import datetime, timedelta

import requests

from constants import *
from utils.state import State
from utils.storage import RedisStorage, BaseStorage
from utils.logger import logger
from celery_config import app


cheapest_prices_cache: dict = {}


FlightDirection: namedtuple = namedtuple('FlightDirection', ['fly_from', 'fly_to'])


def get_dates_for_request() -> tuple:
    """
    Get start and end date interval for request
    :return: start and end dates
    """
    logger.info('Calculating dates for request')
    today: datetime = datetime.utcnow()
    start_date: str = today.strftime(DATE_FORMAT)
    end_date: str = (today + timedelta(days=DAYS_CREATE_CACHE_FOR)).strftime(DATE_FORMAT)
    return start_date, end_date


def check_flight_is_valid(booking_token: str) -> bool:
    """
    Get validity of flights
    :param booking_token: booking token from get request
    :return: check result
    """
    is_flight_valid: bool = False
    params: dict = BOOKING_API_TEMPLATE_PARAMS.copy()
    params.update({'booking_token': booking_token})
    try:
        check_flight_response: dict = requests.get(BOOKING_CHECK_FLIGHTS_ENDPOINT, params=params).json()
        is_flight_valid: bool = not check_flight_response.get('flights_invalid')
    except (requests.ConnectionError, requests.RequestException, requests.HTTPError) as exc:
        logger.warning(f'Failed to check flights with booking_token {booking_token}. Returning default False. {exc}')
    return is_flight_valid


def get_direction_dict_key(flight_direction: namedtuple) -> str:
    """
    Get direction str as key from namedtuple for json dumping
    :param flight_direction: direction named tuple
    :return: str IATA_from-IATA_to
    """
    return f'{flight_direction.fly_from}-{flight_direction.fly_to}'


def update_cache_with_directions(flight_directions: list, cache: dict) -> None:
    """
    Update cache keys which consists from directions
    :param flight_directions: list of namedtuples of directions
    """
    cache.clear()
    for flight_direction in flight_directions:
        flight_direction_key: str = get_direction_dict_key(flight_direction)
        cache[flight_direction_key] = defaultdict(dict)


def get_flight_directions() -> list:
    """
    Get flight directions with named fields with backwards directions if needed
    :return: list of named tuple objects with directions
    """
    logger.info('Gathering popular directions from settings')
    if FLIGHT_DIRECTIONS:
        result: list = []
        for direction in FLIGHT_DIRECTIONS:
            try:
                result.append(FlightDirection(direction[0], direction[1]))
                if direction[2]:
                    result.append((FlightDirection(direction[1], direction[0])))
            except IndexError:
                logger.warning(f'Direction {direction} was missing an required element. Skip and continue')

        update_cache_with_directions(result, cheapest_prices_cache)

        return result


def get_available_flights_for_direction(flight_direction: namedtuple, dates_for_request: tuple) -> list:
    """
    Get available flight between given dates range
    :param flight_direction: from-to
    :param dates_for_request: date_start, date_end
    :return: list of flights
    """
    try:

        available_flights_response: requests.Response = requests.get(SKYPICKER_FLIGHTS_ENDPOINT, params={
            'fly_from': flight_direction.fly_from,
            'fly_to': flight_direction.fly_to,
            'date_from': dates_for_request[0],
            'date_to': dates_for_request[1],
            'sort': 'date',
            'partner': PARTNER_ID,
        })

        result: dict = available_flights_response.json()
        logger.info(f'Got flights data for direction {get_direction_dict_key(flight_direction)}')
        return result.get('data')

    except (requests.ConnectionError, requests.RequestException, requests.HTTPError) as exc:
        logger.warning(f"""Failed to get data for direction {flight_direction.fly_from}-{flight_direction.fly_to} 
                        with dates from {dates_for_request[0]} to {dates_for_request[1]}. Exception: {exc}""")


def add_direction_cheapest_price_to_cache(flight_direction: namedtuple, available_flights: list, cache: dict) -> None:
    """
    Iterate over flights of given direction getting cheapest price for each date at the same time
    :param flight_direction: namedtuple, key in cache
    :param available_flights: list of flights from api response
    """
    logger.info(f'Adding flights data for {get_direction_dict_key(flight_direction)}')
    if available_flights:
        for flight in available_flights:

            flight_date_raw: int = flight.get('dTimeUTC')
            flight_date: str = datetime.fromtimestamp(flight_date_raw).strftime(DATE_FORMAT)

            flight_price: float = flight.get('price')

            flight_booking_token: str = flight.get('booking_token')

            flight_direction_key = get_direction_dict_key(flight_direction)

            current_direction: dict = cache[flight_direction_key]
            current_price: float = current_direction.get(flight_date, {}).get('price')
            if (current_price is None or current_price > flight_price) and check_flight_is_valid(flight_booking_token):
                current_direction[flight_date] = {'booking_token': flight_booking_token, 'price': flight_price}


def save_cache(cache: dict, state: State) -> None:
    """
    Store refresh results for backup and usage
    :param cache: prepared cache
    :param state: state writer
    """
    logger.info(f'Saving cache')
    state.set_state(LAST_RUN_STATE_KEY, datetime.utcnow().strftime(DATE_FORMAT))
    state.set_state(CACHE_KEY, json.dumps(cache))


def need_refresh_cache(state: State) -> bool:
    """
    Only run full refresh once a day to evade API excessive load
    :return: result of check if cache was refreshed today already
    """
    logger.info('Checking if cache already refreshed today')
    is_need_refresh: bool = True
    last_refresh_date: str = state.get_state(LAST_RUN_STATE_KEY)
    now: str = datetime.utcnow().strftime(DATE_FORMAT)
    if last_refresh_date:
        is_need_refresh = now > last_refresh_date
    return is_need_refresh


@app.task
def run():
    state_storage: BaseStorage = RedisStorage(REDIS_HOST, REDIS_PORT)
    state: State = State(state_storage)
    logger.info('Starting refresher')
    if need_refresh_cache(state):
        logger.info('Cache needs refresh')
        flight_directions: list = get_flight_directions()
        if flight_directions:
            dates_for_request: tuple = get_dates_for_request()
            for flight_direction in flight_directions:
                logger.info(f'Handling direction {get_direction_dict_key(flight_direction)}')
                available_flights: list = get_available_flights_for_direction(flight_direction, dates_for_request)
                add_direction_cheapest_price_to_cache(flight_direction, available_flights, cheapest_prices_cache)
            save_cache(cheapest_prices_cache, state)
            logger.info('All done successfully')
    else:
        logger.info('Cache for today already exists. Exiting')
