import json
from collections import namedtuple

from constants import *
from utils.state import State
from utils.storage import RedisStorage, BaseStorage
from cache_tools.cache_refresher import check_flight_is_valid, get_available_flights_for_direction, \
    add_direction_cheapest_price_to_cache, FlightDirection, save_cache
from utils.logger import logger
from celery_config import app


cheapest_prices_cache: dict = {}


@app.task
def run():
    state_storage: BaseStorage = RedisStorage(REDIS_HOST, REDIS_PORT)
    state: State = State(state_storage)
    cache_json: str = state.get_state(CACHE_KEY)
    if cache_json:
        cache: dict = json.loads(cache_json)
        logger.info('Starting validator')
        logger.info('Loaded existing cache')
        for flight_direction_key, flight_direction_data in cache.items():
            logger.info(f'Checking direction {flight_direction_key}')
            for date, flight_data in flight_direction_data.items():
                if not check_flight_is_valid(flight_data.get('booking_token')):
                    logger.info(f'Found invalid flight for {flight_direction_key} on {date}. Replacing')
                    iata_codes: list = flight_direction_key.split('-')
                    flight_direction: namedtuple = FlightDirection(iata_codes[0], iata_codes[1])
                    new_direction_data: list = get_available_flights_for_direction(flight_direction, (date, date))
                    add_direction_cheapest_price_to_cache(flight_direction, new_direction_data, cache)
        save_cache(cache, state)
